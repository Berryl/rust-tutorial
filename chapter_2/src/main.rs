extern crate rand;

use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {

    let (min, max): (u8, u8) = (1, 101);
    let secret: u8 = gen_rand(&min, &max);
    println!("Secret random number: {}", secret); // enable cheats

    let mut is_correct:bool = false;
    while !is_correct {
        println!("Guess a number between {} and {}!", min, max);
        println!("Please input your guess.");

        let mut input = String::new();
        let mut guess: u8 = 0;

        if !read_line(&mut input) 
            || !read_guess(&input, &mut guess)
            { continue; }

        println!("You guessed: {}", guess);
        is_correct = compare(guess, secret);
    }

}

fn read_line(input: &mut String) -> bool {
    match io::stdin().read_line(input) {
        Ok(_s) => {
            return true;
        },
        Err(_) => {
            println!("Failed to read line.");
            return false;
        }
    }
}

fn read_guess(input: &String, guess: &mut u8) -> bool {
    match input.trim().parse() {
        Ok(num) => {
            let temp: u32 = num;
            *guess = temp as u8;
            return true;
        },
        Err(_) => return false
    }
}

fn gen_rand(min: &u8, max: &u8) -> u8 {
    return rand::thread_rng().gen_range(*min, *max);
}

fn compare(guess: u8, secret: u8) -> bool {
    match guess.cmp(&secret) {
        Ordering::Less => println!("Too small!"),
        Ordering::Greater => println!("Too big!"),
        Ordering::Equal => {
            println!("You win!");
            return true;
        }
    }
    return false;
}
